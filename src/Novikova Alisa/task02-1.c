#define _CRT_SECURE_NO_WARNINGS
#define K 40
#define M 2
#define N 256
#include <stdio.h>
#include <string.h>
#include <time.h>
int main()
{
	int length;
	printf("Enter the length of the password, please: ");
	scanf("%i", &length);
	int i, j, num1, num2, num3, takt;
	char pass[N];
	srand(time(0));
	for (i = 0; i < K; ++i) {
		num1 = 0; num2 = 0;  num3 = 0;
		while (num1 == 0 || num2 == 0 || num3 == 0) {
			num1 = 0;  num2 = 0; num3 = 0;
			for (j = 0; j<length; ++j) {
				takt = rand() % 3;
				if (takt == 0) {
					pass[j] = rand() % 27 + 'A';
					num1 = 1;
				}
				if (takt == 1) {
					pass[j] = rand() % 27 + 'a';
					num2 = 1;
				}
				if (takt == 2) {
					pass[j] = rand() % 10 + '0';
					num3 = 1;
				}
			}
		}
		for (j = 0; j < length; ++j)
			printf("%c", pass[j]);
		if ((i + 1) % M == 0)
			printf("\n");
		else
			printf("   ");
	}
	return 0;
}

